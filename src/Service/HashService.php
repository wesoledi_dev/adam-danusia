<?php

namespace App\Service;

class HashService
{
    private int $hash1;
    private int $hash2;
    private int $hash3;

    public function __construct() {
        $this->hash1 = $_ENV['HASH1'];
        $this->hash2 = $_ENV['HASH2'];
        $this->hash3 = $_ENV['HASH3'];
    }

    public function encode(string $param): string
    {
        $offset = rand(0, $this->hash3);
        $result = sprintf('%d', $offset);
        $pattern = '%0' . $this->hash1 . 'd%0' . $this->hash2 . 'd';
        for ($i = 0; $i < strlen($param); $i++) {
            $letter = substr($param, $i, 1);
            $letterAsciiCode = ord($letter) - $offset;
            $randomPrefixValue = rand(10, 99);
            $letterValue = sprintf(
                // '%02d%03d',
                $pattern,
                $randomPrefixValue,
                $letterAsciiCode,
            );
            $result .= $letterValue;
        }
        return $result;
    }

    public function decode(string $param): string
    {
        try {
            $result = '';
            $offset = intval(substr($param, 0, 1));

            $realInput = substr($param, 1);
            $paramRealLength = strlen($realInput) / $this->hash3;

            for ($i = 0; $i < $paramRealLength; $i++) {
                $fragment = substr($realInput, $i * $this->hash3, $this->hash3);
                $asciiPart = substr($fragment, $this->hash1, $this->hash3);
                $letter = chr(intval($asciiPart) + $offset);
                $result .= $letter;
            }
            return $result;
        } catch (\Exception $e) {
            return $param;
        }
    }
}