<?php

namespace App\Service;

class FormPostDataExtractor
{
    private string $rawData;
    private array $readyData;

    public function __construct(string $data)
    {
        $this->rawData = $data;
        $this->readyData = [];
    }

    public function extract(): self
    {
        $postDatas = explode('&', $this->rawData);
        $postData = array_reduce(
            $postDatas,
            static function ($reduced, $item) {
                $keyValue = explode('=', $item);
                $result = [$keyValue[0] => $keyValue[1]];
                return array_merge($result, $reduced ?? []);
            }
        );
        $this->readyData = $postData;

        return $this;
    }

    public function getDataByKey(string $key): string
    {
        return !empty($this->readyData) ? $this->readyData[$key] : '';
    }
}