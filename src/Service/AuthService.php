<?php

namespace App\Service;

use Symfony\Component\HttpFoundation\Cookie;
use App\Entity\Guest;
use App\Service\HashService;
use Doctrine\Persistence\ManagerRegistry;

class AuthService {
    private ManagerRegistry $doctrine;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->doctrine = $doctrine;
    }

    public function checkPass(string $pass): ?array
    {
        $repo = $this->doctrine->getRepository(Guest::class);
        return $repo->findBy([
            'pass' => sha1($pass)
        ]);
    }

    public function createCookie(Guest $entity): Cookie
    {
        $hashService = new HashService();

        $values = [];
        $values[] = $hashService->encode($entity->getName());
        $values[] = $hashService->encode($entity->getPass());
        return Cookie::create('guest', implode('x', $values))
            ->withExpires(strtotime('2021-10-23 16:00'));
    }

    public function getGuestFromCookie($cookies): ?array
    {
        if (!$cookies->has('guest')) {
            return null;
        }

        $hashService = new HashService();
        $cookieValue = $cookies->get('guest');
        $splitted = explode('x', $cookieValue);
        $name = $hashService->decode($splitted[0] ?? '');
        $pass = $hashService->decode($splitted[1] ?? '');

        $repo = $this->doctrine->getRepository(Guest::class);
        return $repo->findBy([
            'pass' => $pass,
            'name' => $name,
        ]);
    }
}