<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Asset\Package;
use Symfony\Component\Asset\VersionStrategy\EmptyVersionStrategy;
use Symfony\Component\HttpFoundation\Request;
use App\Service\AuthService;
use App\Service\FormPostDataExtractor;
use App\Entity\Guest;

class DefaultController extends AbstractController
{
    public function index(Request $request)
    {
        $guest = $this->getGuest($request);
        if (!empty($guest)) {
            return $this->redirectToRoute('details');
        }

        return $this->renderPage('home', [
            'error' => $request->query->get('error'),
        ]);
    }

    public function check(Request $request)
    {
        $authService = new AuthService($this->getDoctrine());

        $postData = explode('=', $request->getContent());
        $pass = $postData[1] ?? '';

        $guest = $authService->checkPass($pass);
        if (empty($guest)) {
            return $this->redirectToRoute('index', ['error' => 1]);
        }

        $cookie = $authService->createCookie($guest[0]);
        $result = $this->redirectToRoute('details');
        $result->headers->setCookie($cookie);
        return $result;
    }

    public function details(Request $request)
    {
        $guest = $this->getGuest($request);
        if (empty($guest)) {
            return $this->redirectToRoute('index', ['error' => 2]);
        }

        return $this->renderPage('details', [
            'guest' => $guest,
            'assets' => $this->getDetailsAssets(),
            'inPersonWords' => $this->getInPersonWords($guest),
        ]);
    }

    public function confirm(Request $request)
    {
        $guest = $this->getGuest($request);
        if (empty($guest)) {
            return $this->redirectToRoute('index', ['error' => 2]);
        }

        $formPostDataExtractor = (new FormPostDataExtractor($request->getContent()))->extract();
        $quantity = (int)$formPostDataExtractor->getDataByKey('qty');
        $guestId = $formPostDataExtractor->getDataByKey('guest');

        if ($guest->getId() !== (int)$guestId) {
            return $this->redirectToRoute('index', ['error' => 2]);
        }

        if ($quantity > 0) {
            $guest->setConfirmed(true);
            $guest->setUnconfirmed(false);
            $guest->setQuantity($quantity);
        } else {
            $guest->setConfirmed(false);
            $guest->setUnconfirmed(true);
        }
        
        $this->saveGuest($this->getDoctrine()->getManager(), $guest);

        return $this->redirectToRoute('details');
    }

    public function saveContact(Request $request)
    {
        $guest = $this->getGuest($request);
        if (empty($guest)) {
            return $this->redirectToRoute('index', ['error' => 2]);
        }

        $formPostDataExtractor = (new FormPostDataExtractor($request->getContent()))->extract();
        $contact = $formPostDataExtractor->getDataByKey('contact');
        $guestId = $formPostDataExtractor->getDataByKey('guest');

        if ($guest->getId() !== (int)$guestId) {
            return $this->redirectToRoute('index', ['error' => 2]);
        }

        $guest->setConfirmed(true);
        $guest->setContact((int)$contact);
        $this->saveGuest($this->getDoctrine()->getManager(), $guest);

        return $this->redirectToRoute('details');
    }

    public function renderPage(string $page, array $params)
    {
        $assets = $this->getAssets();
        if (array_key_exists('assets', $params)) {
            foreach($assets as $key => $asset) {
                $params['assets'][$key] = $asset;
            }
        } else {
            $params['assets'] = $assets;
        }
        $pagePath = 'pages/' . $page . '.html.twig';
        return $this->render($pagePath, $params);
    }

    private function getGuest(Request $request): ?Guest
    {
        $authService = new AuthService($this->getDoctrine());
        $guest = $authService->getGuestFromCookie($request->cookies);

        if (empty($guest)) {
            return null;
        }
        return $guest[0];
    }

    private function saveGuest($entityManager, Guest $guest): void
    {
        $entityManager = $this->getDoctrine()->getManager();
        $entityManager->persist($guest);
        $entityManager->flush();
    }

    private function getAssets(): array
    {
        $package = new Package(new EmptyVersionStrategy());
        return [
            'photo_us' => $package->getUrl('/images/we.jpg'),
        ];
    }

    private function getDetailsAssets(): array
    {
        $package = new Package(new EmptyVersionStrategy());
        return [
            'calendar' => $package->getUrl('/images/calendar.svg'),
            'church' => $package->getUrl('/images/church.svg'),
            'users' => $package->getUrl('/images/users.svg'),
            'champagne' => $package->getUrl('/images/champagne.svg'),
        ];
    }

    private function getInPersonWords(Guest $guest): array
    {
        $hello = 'Cześć';
        $you = 'Cię';
        $youWill = 'będziesz';

        if ($guest->getHello() === 1) {
            $hello = 'Dzień dobry';
        }

        if ($guest->getQuantity() > 1) {
            $you = 'Was';
            $youWill = 'będziecie';
        }

        return [
            'you' => $you,
            'youWill' => $youWill,
            'hello' => $hello,
        ];
    }
}
